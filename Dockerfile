FROM ubuntu:17.10
MAINTAINER TZer0

RUN apt-get update
RUN apt-get install -y python3.6 python3-pip
RUN pip3 install disco-py ujson pony
RUN mkdir /acceptbot
ADD *.py /acceptbot/

CMD cd /acceptbot && python3 -m disco.cli --config config.json
